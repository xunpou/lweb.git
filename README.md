### 非常轻量级的webAPI
简单实现web接口

#### 简单示例

1.先实现RequestHandle的接口

    public class MyRequestHandle extends RequestHandle {
         @Override
         public void taskExecute(RequestPacket requestPacket, ResponsePacket responsePacket) {
             System.out.println(Thread.currentThread().getId());
             responsePacket.write(LocalDateTime.now().toString().getBytes(StandardCharsets.UTF_8));
         }
    }

2.传入RequestHandle

    public class test {
        public static void main(String[] args) throws Exception{
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5,10,0, TimeUnit.SECONDS,new LinkedBlockingDeque<>());
            WebServer server = new WebServer(MyRequestHandle.class,8080,threadPoolExecutor);
            server.start();
        }
    }