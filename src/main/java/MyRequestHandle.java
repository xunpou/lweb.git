import com.xunpou.net.RequestHandle;
import com.xunpou.net.RequestPacket;
import com.xunpou.net.ResponsePacket;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

/**
 * @author zq.remote
 * @since 2022/11/23 14:25
 */
public class MyRequestHandle extends RequestHandle {
    @Override
    public void taskExecute(RequestPacket requestPacket, ResponsePacket responsePacket) {
        System.out.println(Thread.currentThread().getId());
        responsePacket.write(LocalDateTime.now().toString().getBytes(StandardCharsets.UTF_8));
    }
}
