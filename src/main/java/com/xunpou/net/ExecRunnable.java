package com.xunpou.net;



import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExecRunnable implements Runnable {

    private final Socket clientSocket;
    private final Class<? extends RequestHandle> requestHandle;

    public ExecRunnable(Socket socket, Class<? extends RequestHandle> handle) {
        this.clientSocket = socket;
        this.requestHandle = handle;
    }
    @Override
    public void run() {
        InputStream inputStream;
        OutputStream outStream;
        ResponsePacket responsePacket = new ResponsePacket();
        Map<String, String> param;
        try {
            inputStream = clientSocket.getInputStream();
            //限制参数最大长度
            byte[] buff = new byte[2048];
            int n = inputStream.read(buff);
            boolean errorFlag = false;
            int firstLineEndIndex;
            //获取第一行结束下标
            for (firstLineEndIndex = 0; firstLineEndIndex < n; firstLineEndIndex++) {
                if (buff[firstLineEndIndex] < 0) {
                    errorFlag = true;
                    break;
                }
                if (buff[firstLineEndIndex] == '\n' || buff[firstLineEndIndex] == '\r') {
                    break;
                }
            }
            if (errorFlag) {
                responsePacket.write("{'error':'参数需要url编码(utf-8)'}".getBytes(StandardCharsets.UTF_8));
            } else {
                if (firstLineEndIndex == buff.length){
                    firstLineEndIndex++;
                }
                param = getParams(new String(buff, 0, firstLineEndIndex));
                try {
                    RequestPacket requestPacket = new RequestPacket(param, clientSocket.getRemoteSocketAddress().toString());
                    //执行
                    RequestHandle requestHandleInstance = requestHandle.getConstructor().newInstance();
                    requestHandleInstance.taskExecute(requestPacket, responsePacket);
                } catch (Exception e) {
                    responsePacket.write(e.toString().getBytes(StandardCharsets.UTF_8));
                    e.printStackTrace();
                }
            }
            outStream = clientSocket.getOutputStream();
            writeTo(responsePacket, outStream);
            outStream.flush();
            outStream.close();
            inputStream.close();
            outStream.close();
            clientSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     *  获取url编码的参数
     */
    private static Map<String, String> getParams(String s) throws Exception {
        Map<String, String> params = new HashMap<>();
        Matcher matcher = Pattern.compile("([\\w%+]+)=([^ &]*)").matcher(s);
        while (matcher.find()) {
            params.put(URLDecoder.decode(matcher.group(1),"UTF-8"), URLDecoder.decode(matcher.group(2), "UTF-8"));
        }
        return params;
    }

    private static void writeTo(ResponsePacket responsePacket, OutputStream out) throws Exception {
        List<String> headers = responsePacket.getHeaders();
        StringBuilder builder = new StringBuilder();
        builder.append("HTTP/1.1 200").append("\r\n");
        builder.append("Connection:close").append("\r\n");
        if (headers != null) {
            for (String header : headers) {
                builder.append(header).append("\r\n");
            }
        }
        builder.append("Content-Length:")
                .append(responsePacket.getBody().length)
                .append("\r\n\r\n");
        out.write(builder.toString().getBytes(StandardCharsets.UTF_8));
        out.write(responsePacket.getBody());
    }


}
