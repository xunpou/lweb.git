package com.xunpou.net;


public abstract  class RequestHandle {

    public abstract void taskExecute(RequestPacket requestPacket, ResponsePacket responsePacket);

}
