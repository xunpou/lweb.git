package com.xunpou.net;

import java.util.LinkedList;
import java.util.List;

/**
 * @author leizhenzi
 */

public class ResponsePacket {
    private List<String> headers;
    private byte[] body;

    public byte[] getBody() {
        return body;
    }

    public List<String> getHeaders() {
        return headers;
    }

    public void addHeader(String header){
        if(headers==null){
            headers = new LinkedList<>();
        }
        headers.add(header);
    }
    public void write(byte[] body){
        this.body = body;
    }
}
