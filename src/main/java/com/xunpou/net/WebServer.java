package com.xunpou.net;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class WebServer {
    private final Class<? extends RequestHandle> handle;
    private final int port;
    private final ThreadPoolExecutor threadPoolExecutor;
    private static ThreadPoolExecutor  createDefaultThreadPool(){
        return new ThreadPoolExecutor(20,100,60, TimeUnit.SECONDS,new LinkedBlockingDeque<>(20));
    }
    public void start() throws Exception{
        ServerSocket serverSocket = new ServerSocket(port);
      while (true){
            Socket clientSocket = serverSocket.accept();
            System.out.println("-->"+clientSocket.getRemoteSocketAddress());
            threadPoolExecutor.execute(new ExecRunnable(clientSocket,handle));
            System.out.println("current:"+threadPoolExecutor.getPoolSize());
        }
    }


    public WebServer(Class<? extends RequestHandle> handle, int port) throws Exception{
        if(handle.equals(RequestHandle.class)){
          throw new Exception("不支持原始RequestHandle.class,需要实现RequestHandle.class的派生类");
        }
        this.handle = handle;
        this.port=port;
        this.threadPoolExecutor = createDefaultThreadPool();
    }

    public WebServer(Class<? extends RequestHandle> handle, int port, ThreadPoolExecutor threadPoolExecutor) throws Exception{
        if(handle.equals(RequestHandle.class)){
            throw new Exception("不支持原始RequestHandle.class,需要实现RequestHandle.class的派生类");
        }
        this.handle = handle;
        this.port=port;
        this.threadPoolExecutor = threadPoolExecutor;
    }

}
