package com.xunpou.net;

import java.util.Map;



public class RequestPacket {
    private final Map<String, String> params;
    private final String remoteIp;

    public RequestPacket(Map<String, String> params, String remoteIp) {
        this.params = params;
        this.remoteIp = remoteIp;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public String getRemoteIp() {
        return remoteIp;
    }
}
