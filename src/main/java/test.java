import com.xunpou.net.WebServer;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author zq.remote
 * @since 2022/11/23 14:25
 */
public class test {
    public static void main(String[] args) throws Exception{
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5,10,0, TimeUnit.SECONDS,new LinkedBlockingDeque<>());
        WebServer server = new WebServer(MyRequestHandle.class,8080,threadPoolExecutor);
        server.start();
    }
}
